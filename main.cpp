#include "expert_system.hpp"

int main(int argc, char **argv) {
	std::list< std::string >	equations;
	std::vector<char>			true_facts;
	std::vector<char>			false_facts;
	std::vector<char>			unknown_facts;
	std::vector<char>			needed_facts;
	std::vector<char>			undefined_facts;
	std::vector<int>			results;
	int 						unknown_facts_count;

	int i = 0;

	if (argc != 2) {
		std::cout << "There must be given one file in arguments" << std::endl;
		return (1);
	}

	unknown_facts_count = 0;
	Parser parser(argv[1]);
	parser.getAllEquations();

	equations = parser.getListEquations();
	true_facts = parser.getInitFacts();
	needed_facts = parser.getNeeded_facts();
	compare c;
	equations.sort(c);

	results.reserve(equations.size());

	while (!check_find_arguments(needed_facts, true_facts, false_facts)) {

		for (auto iter : equations) {
			std::cout << iter << std::endl;
			unknown_facts = find_unnown_facts(iter, true_facts, false_facts);
			results.push_back(solve(iter, true_facts, false_facts));
		}

		//break after certain number of cycles
		unknown_facts = find_all_unknown_facts(equations, true_facts, false_facts);
		if (unknown_facts_count == unknown_facts.size()) {
			for (auto fact : unknown_facts) {
				if (choose_fact_undefined_or_false(results, equations, fact))
					false_facts.push_back(fact);
				else
					undefined_facts.push_back(fact);
			}
			break ;
		}
		unknown_facts_count = unknown_facts.size();
		results.clear();
	}

	//Print TRUE facts after each iteration
	std::cout << "True facts ";
	for (auto elem : true_facts)
		std::cout << elem << " ";
	std::cout << std::endl;

	//Print FALSE facts after each iteration
	std::cout << "False facts ";
	for (auto elem : false_facts)
		std::cout << elem << " ";
	std::cout << std::endl;

	//Print UNDEFINED facts after each iteration
	std::cout << "Undefined facts ";
	for (auto elem : undefined_facts)
		std::cout << elem << " ";
	std::cout << std::endl;
	return (0);
}

int
check_find_arguments(std::vector<char> const &needed_facts, std::vector<char> const &true_facts,
					 std::vector<char> const &false_facts) {
	int count;

	count = 0;
	for (auto elem : needed_facts) {
		if (std::find(true_facts.begin(), true_facts.end(), elem) != true_facts.end() ||
			std::find(false_facts.begin(), false_facts.end(), elem) != false_facts.end())
		{
			count++;
		}
	}

	return (count == needed_facts.size() ? 1 : 0);
}

int
choose_fact_undefined_or_false(std::vector<int> equations_result, std::list<std::string> equations, char fact) {
	int i;

	i = 0;
	for (auto equation : equations) {
		for (auto symbol : equation) {
			if (fact == symbol) {
				if (equations_result[i]) {
					return (1);
				}
			}
		}
		i++;
	}
	return (0);
}

std::vector<char>
find_all_unknown_facts(std::list<std::string> const & equations, std::vector<char> & true_facts,
					   std::vector<char> & false_facts) {
	std::vector<char> unknown_facts;

	for (auto equation : equations) {
		for (auto iter = equation.begin(); iter != equation.end(); iter++) {
			if (std::find(true_facts.begin(), true_facts.end(), *iter) == true_facts.end() &&
				std::find(false_facts.begin(), false_facts.end(), *iter) == false_facts.end() &&
				std::find(unknown_facts.begin(), unknown_facts.end(), *iter) == unknown_facts.end() &&
				isupper(*iter))
			{
				unknown_facts.push_back(*iter);
			}
		}
	}
	return unknown_facts;
}