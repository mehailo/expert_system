//
// Created by Mykhailo Frankevich on 6/30/17.
//

#ifndef EXPERT_SYSTEM_EXPERT_SYSTEM_HPP
#define EXPERT_SYSTEM_EXPERT_SYSTEM_HPP
#include <iostream>
#include <string>
#include "Parser.hpp"
#include "Solver.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>
#include <sstream>
#include <iostream>
#include <sstream>

int
solve(std::string &equation, std::vector<char> &true_facts, std::vector<char> &false_facts);

std::vector<char>
find_unnown_facts(std::string &equation, std::vector<char> & true_facts, std::vector<char> & false_facts);

std::bitset<26>
form_known_facts(std::vector<char> &true_facts);

void
form_full_facts_bitset(std::bitset<26> &facts, std::bitset<26> const &facts_generated,
					   std::vector<char> const &unknown_facts);

void
add_facts(std::list< std::bitset<26> > &buff_facts, std::vector<char> const &unknown_facts,
		  std::vector<char> &true_facts, std::vector<char> &false_facts);

int
check_find_arguments(std::vector<char> const &needed_facts, std::vector<char> const &true_facts,
					 std::vector<char> const &false_facts);

void
operate_single_fact(std::list< std::bitset<26> > &buff_facts, char const unknown_fact,
					std::vector<char> &true_fact, std::vector<char> &false_facts);

bool
compareLen(const std::string& a, const std::string& b);

int
choose_fact_undefined_or_false(std::vector<int> equations, std::list<std::string>, char fact);

int
choose_fact_undefined_or_false(std::vector<int> equations_result, std::list<std::string> equations, char fact);

std::vector<char>
find_all_unknown_facts(std::list<std::string> const & equations, std::vector<char> & true_facts,
					   std::vector<char> & false_facts);

struct compare {
	bool operator()(const std::string& first, const std::string& second) {
		return first.size() < second.size();
	}
};

#endif //EXPERT_SYSTEM_EXPERT_SYSTEM_HPP
