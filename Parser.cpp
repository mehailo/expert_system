//
// Created by Mykhailo Frankevich on 6/20/17.
//

#include "expert_system.hpp"

Parser::Parser() : filename(""){
	return ;
}

Parser::Parser(std::string filename) : filename(filename) {
	return ;
}

Parser::~Parser() {
	return ;
}

Parser &Parser::operator=(Parser const & rhs) {
	this->filename = rhs.filename;

	return *this;
}

Parser::Parser(Parser const & other) {
	*this = other;
}

int Parser::getEquations()
{
	std::vector<char> equation;
	std::string line(getNextLine());

	if (line.empty())
		return 0;
	if (line.empty())
		return 0;
	cutComment(line);
	if (line[0] == '=')
		parse_init_facts(line);
	else if (line[0] == '?')
		parse_needed_facts(line);
	else
		parse_equation(line);

	return 1;
}

std::string Parser::getNextLine()
{
	std::string line;
	static std::fstream	file(filename);

	getline(file, line);
	return line;
}

void Parser::substituteSubstr(std::string &str, std::string old_str,
							  std::string new_str)
{
	size_t index = 0;

	while ((index = str.find(old_str, index)) != std::string::npos) {
		str.replace(index, old_str.length(), new_str);
		index += new_str.length();
	}
}

void	Parser::cutComment(std::string &str)
{
	size_t index = 0;

	if ((index = str.find("#", index)) != std::string::npos)
		str = strndup(str.c_str(), index);

	return ;
}

void Parser::parse_init_facts(std::string line)
{
	size_t index = 1;

	while (isalpha(line[index])) {
		init_facts.push_back(line[index++]);
	}
	return ;
}

void Parser::parse_needed_facts(std::string line)
{
	size_t index = 1;

	while (isalpha(line[index])) {
		needed_facts.push_back(line[index++]);
	}
	return ;
}

void Parser::parse_equation(std::string line)
{
	substituteSubstr(line, "<=>", "=");
	substituteSubstr(line, "=>", ">");
	substituteSubstr(line, " ", "");

	if (line.size() == 0)
		return ;

	full_equations.push_back(line);
	std::vector<char> equation(line.begin(), line.end());
	equations.push_front(equation);
}

std::list<std::string> Parser::getListEquations()
{
	return full_equations;
}

std::vector<char> Parser::getInitFacts()
{
	return init_facts;
}

const std::vector<char> &Parser::getNeeded_facts() const {
	return needed_facts;
}

void Parser::getAllEquations()
{
	while(getEquations())
		;
}
