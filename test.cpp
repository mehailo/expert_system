

const unsigned g_unValues = 4;

bool expression(int values[])
{
	return (values[3] * values[0] * ((values[1] || values[2]) * !(values[1] * values[2])));
}

void truth_table(bool (*func)(int[]), unsigned nvalues);

int main(int argc, char** argv)
{
	truth_table(expression, g_unValues);

	return 0;
}

void truth_table(bool (*func)(int[]), unsigned nvalues)
{
	// assert(pow(2, nvalues) <= sizeof(unsigned));

	int values[4];
	unsigned individuals[4];
	unsigned result = 0;

	std::fill_n(individuals, nvalues, 0);

	// Display truth table header
	for (unsigned j = 0; j < nvalues; j++) std::cout << char('A' + j) << ' ';
	std::cout << "| Result" << std::endl;

	for (unsigned i = 1; i <= pow(2, nvalues); i++)
	{
		for (unsigned j = 0; j < nvalues; j++)
		{
			values[j] = i & 0x1<<j;
			if (values[j]) individuals[j] |= 0x1<<i;
		}

		bool eval = func(values);
		if (eval) result |= 0x1 << i;

		// Display truth table entry
		for (unsigned j = 0; j < nvalues; j++) std::cout << !!values[j] << ' ';
		std::cout << "| " << eval << std::endl;
	}

	for (unsigned j = 0; j < nvalues; j++)
	{
		if (result != individuals[j]) continue;
		std::cout << "Expression equivalence: " << char('A'+j) << std::endl;
		break;
	}
}
