
#include "expert_system.hpp"

int
solve(std::string &equation, std::vector<char> &true_facts, std::vector<char> &false_facts) {
	std::bitset<26>						facts;
	std::list< std::bitset<26> >		buff_facts;
	std::vector<char>					unknown_facts;
	std::stringstream					expr_ss(equation);
	int 								return_val;

	facts = form_known_facts(true_facts);
	return_val = 0;

	unknown_facts = find_unnown_facts(equation, true_facts, false_facts);
	if (unknown_facts.size() == 0)
		return (0);

	Expr expr(expr_ss);

	for (int mask=0; mask!=(1<<unknown_facts.size()); mask++) {
		std::bitset<26> facts_generated(mask);
		form_full_facts_bitset(facts, facts_generated, unknown_facts);
		std::cout << facts.to_string() << " -> ";
		std::cout << expr.getValue(facts) << std::endl;
		if (expr.getValue(facts)) {
			return_val++;
			buff_facts.push_back(facts);
		}
	}
	if (!buff_facts.empty())
		add_facts(buff_facts, unknown_facts, true_facts, false_facts);
    return (return_val);
}

void
add_facts(std::list< std::bitset<26> > &buff_facts, std::vector<char> const &unknown_facts,
		  std::vector<char> &true_facts, std::vector<char> &false_facts) {
	for (auto elem : unknown_facts) {
		operate_single_fact(buff_facts, elem, true_facts, false_facts);
	}
}

void
operate_single_fact(std::list< std::bitset<26> > &buff_facts, char const unknown_fact,
					std::vector<char> &true_fact, std::vector<char> &false_facts) {
	char buf_symbol;

	buf_symbol = -128;
	for (auto node : buff_facts) {
		if (buf_symbol == -128)
			buf_symbol = node[unknown_fact - 'A'];
		else if (buf_symbol != node[unknown_fact - 'A'])
			return ;
	}
	if (buf_symbol == 0)
		false_facts.push_back(unknown_fact);
	else
		true_fact.push_back(unknown_fact);
}

void
form_full_facts_bitset(std::bitset<26> &facts, std::bitset<26> const &facts_generated,
					   std::vector<char> const &unknown_facts) {
	int i = 0;
	for (auto elem : unknown_facts) {
		std::cout << elem << " <- element; " << facts_generated[i] << " <- its bit" << std::endl;
		facts[elem - 'A'] = facts_generated[i];
		i++;
	}
}

std::bitset<26>
form_known_facts(std::vector<char> &true_facts) {
	std::bitset<26> result;

	for (auto elem : true_facts){
		result[elem - 'A'] = 1;
	}
	return result;
}

std::vector<char>
find_unnown_facts(std::string &equation, std::vector<char> & true_facts, std::vector<char> & false_facts) {
	std::vector<char> unknown_facts;
	for (auto iter = equation.begin(); iter != equation.end(); iter++) {
		if (std::find(true_facts.begin(), true_facts.end(), *iter) == true_facts.end() &&
			std::find(false_facts.begin(), false_facts.end(), *iter) == false_facts.end() &&
			std::find(unknown_facts.begin(), unknown_facts.end(), *iter) == unknown_facts.end() &&
			isupper(*iter))
		{
			unknown_facts.push_back(*iter);
		}
	}
	return unknown_facts;
}