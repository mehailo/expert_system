//
// Created by Mykhailo Frankevich on 6/20/17.
//

#ifndef EXPERT_SYSTEM_PARSER_HPP
#define EXPERT_SYSTEM_PARSER_HPP


#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <list>

class Parser
{
public:
	Parser();
	Parser(std::string filename);

	~Parser();

	Parser &operator=(Parser const & rhs);
	Parser(Parser const & other);

	void					getAllEquations();
	std::list<std::string>	getListEquations();
	std::vector<char>		getInitFacts();
	const std::vector<char> &getNeeded_facts() const;

private:
	std::string		filename;

	int					getEquations();
	std::string			getNextLine();
	void 				substituteSubstr(std::string &str,
										 std::string old_str,
										 std::string new_str);
	void				cutComment(std::string &str);
	void 				parse_init_facts(std::string line);
	void 				parse_equation(std::string line);
	void 				parse_needed_facts(std::string line);

	std::vector<char>	needed_facts;
	std::list< std::vector<char> > equations;
	std::list< std::string > full_equations;
	std::vector<char>	init_facts;
};


#endif //EXPERT_SYSTEM_PARSER_HPP
